Dockerfile for a uni project 

Contains:

  * g++
  * make

Instructions
------------

### Interactive

To pull this container from the Docker registry:

```
$ docker pull ognarb/fs-docker
```

or

```
$ docker pull registry.gitlab.com/fs-docker/docugen
```

### Gitlab CI runner

This container can also be used as a Gitlab CI runner. Use the following
instructions inside `.gitlab-ci.yml` for either `pandoc`:

```
image: registry.gitlab.com/ognarb/fs-docker:latest

pages:
  script:
  - make
  artifacts:
    paths:
    - public
  only:
  - master
```

Authors
-------

You can contact me on the fediverse [@carl@linuxrocks.online](https://linuxrocks.online/@carl).
