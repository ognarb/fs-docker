FROM ubuntu:latest
MAINTAINER Carl Schwan <schwancarl@protonmail.com>

ENV DEBIAN_FRONTEND=noninteractive

RUN rm /bin/sh && ln -s /bin/bash /bin/sh && \
    sed -i 's/^mesg n$/tty -s \&\& mesg n/g' /root/.profile

RUN apt-get update

RUN apt-get install -y build-essential curl git gcc g++

RUN mkdir -p /workspace
VOLUME ["/workspace"]
WORKDIR /workspace

# Define default command.
CMD ["bash"]
